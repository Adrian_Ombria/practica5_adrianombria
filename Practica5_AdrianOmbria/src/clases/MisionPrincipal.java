package clases;

public class MisionPrincipal extends Misiones{

	private String tipo;
	private String descripcion;
	
	public MisionPrincipal(String nombre, String dificultad, int tiempo, String recompensa, String tipo, String descripcion) {
		
		super(nombre, dificultad, tiempo, recompensa);
		this.tipo = tipo;
		this.descripcion = descripcion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String toString() {
		return super.toString() + tipo + " " + descripcion;
	}
}


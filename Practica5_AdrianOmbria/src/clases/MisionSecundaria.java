package clases;

public class MisionSecundaria extends Misiones{

	private int experienciaObtenida;
	private int oroObtenido;
	
	public MisionSecundaria(String nombre, String dificultad, int tiempo, String recompensa, int experienciaObtenida, int oroObtenido) {
		super(nombre, dificultad, tiempo, recompensa);
		this.experienciaObtenida = experienciaObtenida;
		this.oroObtenido = oroObtenido;
	}

	public int getExperienciaObtenida() {
		return experienciaObtenida;
	}

	public void setExperienciaObtenida(int experienciaObtenida) {
		this.experienciaObtenida = experienciaObtenida;
	}

	public int getOroObtenido() {
		return oroObtenido;
	}

	public void setOroObtenido(int oroObtenido) {
		this.oroObtenido = oroObtenido;
	}
	
}

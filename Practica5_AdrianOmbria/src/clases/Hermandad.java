package clases;

public class Hermandad {


	Personaje[] personaje;
	MisionPrincipal[] misionPrincipal;
	MisionSecundaria[] misionSecundaria;
	
	public Hermandad(int cantidadPersonajes, int cantidadMisionesPrincipales, int cantidadMisionesSecundarias) {
		
		this.personaje = new Personaje[cantidadPersonajes];
		this.misionPrincipal = new MisionPrincipal[cantidadMisionesPrincipales];
		this.misionSecundaria = new MisionSecundaria[cantidadMisionesSecundarias];
	}
	
	/*Personaje*/
	
	public Personaje[] altaPersonaje(String nombre, int nivel, String clase) {
		for(int i = 0; i < personaje.length; i++) {
			
			if(personaje[i] == null) {
				personaje[i] = new Personaje(nombre, nivel, clase);
				
				return personaje;
			}
		}
		
		return null;
	}
	
	public void listarPersonaje() {
		for(int i = 0; i < personaje.length; i++) {
			if(personaje[i] != null) {
				System.out.println(personaje[i].toString());
			}
		}
	}
	
	/*Mision principal*/
	
	public MisionPrincipal[] altaMisionPrincipal(String nombre, String dificultad, int tiempo, String recompensa, String tipo, String descripcion) {
		for(int i = 0; i < misionPrincipal.length; i++) {
			
			if(misionPrincipal[i] == null) {
				misionPrincipal[i] = new MisionPrincipal(nombre, dificultad, tiempo, recompensa, tipo, descripcion);
				
				return misionPrincipal;
			}
		}
		
		return null;
	}
	
	public MisionPrincipal buscarMisionPrincipal(String nombre) {
		for(int i = 0; i < misionPrincipal.length; i++) {
			
			if(misionPrincipal[i] != null) {
				
				if(nombre.equals(misionPrincipal[i].getNombre())) {
					
					return misionPrincipal[i];
				}
			}
		}
		
		return null;
	}
	
	public void eliminarMisionPrincipal(String nombre) {
		for(int i = 0; i < misionPrincipal.length; i++) {
			
			if(misionPrincipal[i] != null) {
				
				if(nombre.equals(misionPrincipal[i].getNombre())) {
					
					misionPrincipal[i] = null;
				}
			}
		}
	}
	
	public void listarMisionPrincipal() {
		
		for(int i = 0; i < misionPrincipal.length; i++) {
			if(misionPrincipal[i] != null) {
				System.out.println(misionPrincipal[i].toString());
			}
		}
	}
	
	public void cambiarMisionPrincipal(String nombre, String nombreCambiar, String dificultad, int tiempo, String recompensa, String tipo, String descripcion) {
		
		for(int i = 0; i < misionPrincipal.length; i++) {
			if(misionPrincipal[i] != null) {
				
				if(nombre.equals(misionPrincipal[i].getNombre())) {
					misionPrincipal[i].setNombre(nombreCambiar);
					misionPrincipal[i].setDificultad(dificultad);
					misionPrincipal[i].setTiempo(tiempo);
					misionPrincipal[i].setRecompensa(recompensa);
					misionPrincipal[i].setTipo(tipo);
					misionPrincipal[i].setDescripcion(descripcion);
					
					System.out.println("Cambios realizados");
					
					System.out.println(misionPrincipal[i].toString());
				}
			}
		}
	}
	
	public void listarMisionPrincPorDificultad(String dificultad) {
		
		for(int i = 0; i < misionPrincipal.length; i++) {
			if(misionPrincipal[i] != null) {
				
				if(dificultad.equals(misionPrincipal[i].getDificultad())) {
					System.out.println(misionPrincipal[i]);
				}
			}
		}	
	}
	
	
	/*Mision secundaria*/
	
	public MisionSecundaria[] altaMisionSecundaria(String nombre, String dificultad, int tiempo, String recompensa, int experienciaObtenida, int oroObtenido) {
		for(int i = 0; i < misionSecundaria.length; i++) {
			
			if(misionSecundaria[i] == null) {
				misionSecundaria[i] = new MisionSecundaria(nombre, dificultad, tiempo, recompensa, experienciaObtenida, oroObtenido);
				
				return misionSecundaria;
			}
		}
		
		return null;
	}
	
	public MisionSecundaria buscarMisionSecundaria(String nombre) {
		for(int i = 0; i < misionSecundaria.length; i++) {
			
			if(misionSecundaria[i] != null) {
				
				if(nombre.equals(misionSecundaria[i].getNombre())) {
					
					return misionSecundaria[i];
				}
			}
		}
		
		return null;
	}
	
	public void eliminarMisionSecundaria(String nombre) {
		for(int i = 0; i < misionSecundaria.length; i++) {
			
			if(misionSecundaria[i] != null) {
				
				if(nombre.equals(misionSecundaria[i].getNombre())) {
					
					misionSecundaria[i] = null;
				}
			}
		}
	}
	
	public void listarMisionSecundaria() {
		
		for(int i = 0; i < misionSecundaria.length; i++) {
			if(misionSecundaria[i] != null) {
				System.out.println(misionSecundaria[i].toString());
			}
		}
	}
	
	public void cambiarMisionSecundaria(String nombre, String nombreCambiar, String dificultad, int tiempo, String recompensa, int experienciaObtenida, int oroObtenido) {
		
		for(int i = 0; i < misionSecundaria.length; i++) {
			if(misionSecundaria[i] != null) {
				
				if(nombre.equals(misionSecundaria[i].getNombre())) {
					misionSecundaria[i].setNombre(nombreCambiar);
					misionSecundaria[i].setDificultad(dificultad);
					misionSecundaria[i].setTiempo(tiempo);
					misionSecundaria[i].setRecompensa(recompensa);
					misionSecundaria[i].setExperienciaObtenida(experienciaObtenida);;
					misionSecundaria[i].setOroObtenido(oroObtenido);;
					
					System.out.println("Cambios realizados");
					
					System.out.println(misionSecundaria[i].toString());
				}
			}
		}
	}
	
	public void listarMisionSecunPorDificultad(String dificultad) {
		
		for(int i = 0; i < misionSecundaria.length; i++) {
			if(misionSecundaria[i] != null) {
				
				if(dificultad.equals(misionSecundaria[i].getDificultad())) {
					System.out.println(misionSecundaria[i]);
				}
			}
		}	
	}
	
}

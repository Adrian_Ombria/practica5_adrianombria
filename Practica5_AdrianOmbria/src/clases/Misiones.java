package clases;

public class Misiones {

	private String nombre;
	private String dificultad;
	private int tiempo;
	private String recompensa;
	
	public Misiones(String nombre, String dificultad, int tiempo, String recompensa) {
		this.nombre = nombre;
		this.dificultad = dificultad;
		this.tiempo = tiempo;
		this.recompensa = recompensa;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDificultad() {
		return dificultad;
	}
	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}
	public int getTiempo() {
		return tiempo;
	}
	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}
	public String getRecompensa() {
		return recompensa;
	}
	public void setRecompensa(String recompensa) {
		this.recompensa = recompensa;
	}
	
	public String toString(){
		return nombre + " " + dificultad + " " + tiempo + " " + recompensa;
	}
	
}

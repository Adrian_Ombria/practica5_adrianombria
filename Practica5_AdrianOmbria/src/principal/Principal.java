package principal;

import java.util.Scanner;

import clases.Hermandad;

public class Principal {

public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int condicion = 0;
		
		System.out.println("Misiones principales totales en la hermandad");
		int numMisionPrinc = input.nextInt();
		
		System.out.println("Misiones secundarias totales en la hermandad");
		int numMisionSec = input.nextInt();
		
		System.out.println("Personajes totales en la hermandad");
		int numPers = input.nextInt();
		
		Hermandad programacionLovers = new Hermandad(numMisionPrinc, numMisionSec, numPers);
		
		do {
			
			System.out.println("Elige una de estas opciones: ");
			
			/*Mision principal*/
			
			System.out.println("1- Dar de alta una mision principal");
			System.out.println("2- Buscar mision principal por nombre");
			System.out.println("3- Eliminar mision principal");
			System.out.println("4- Listar misiones principales");
			System.out.println("5- Cambiar el nombre de una mision principal");
			System.out.println("6- Listar misiones principales por dificultad");
			
			/*Mision secundaria*/
			
			System.out.println("7- Dar de alta una mision secundaria");
			System.out.println("8- Buscar mision secundaria por nombre");
			System.out.println("9- Eliminar mision secundaria");
			
			/*Personaje*/
			
			System.out.println("10- Dar de alta a personaje");
			System.out.println("11- Listar personajes");
			
			/*Salida del programa*/
			
			System.out.println("12- Salir del programa");
			
			condicion = input.nextInt();
			input.nextLine();
			
			switch(condicion) {
				
			case 1:
				
				System.out.println("�Cuantas misiones principales quieres poner?");
				int numMisionPrincAlta = input.nextInt();
				
				input.nextLine();
				
				int totalMisionPrinc = 1;
				
				for (int i = 0; i < numMisionPrincAlta; i++){
					
					if(numMisionPrincAlta <= numMisionPrinc) {
						
						System.out.println("Rellene los campos de la mision principal " + totalMisionPrinc);
						
						System.out.println("Ingrese el nombre");
						String nombre = input.nextLine();
						
						System.out.println("Ingrese la dificultad");
						String dificultad = input.nextLine();
						
						System.out.println("Ingrese el tiempo");
						int tiempo = input.nextInt();
						
						System.out.println("Ingrese la recompensa");
						String recompensa = input.nextLine();
						
						System.out.println("Ingrese el tipo");
						String tipo = input.nextLine();
						
						System.out.println("Ingrese la descripcion");
						String descripcion = input.nextLine();
						
						input.nextLine();
						
						programacionLovers.altaMisionPrincipal(nombre, dificultad, tiempo, recompensa, tipo, descripcion);
						totalMisionPrinc++;
						
					}else {
						System.out.println("No se pueden a�adir mas misiones principales");
					}
				}
				
				break;
				
			case 2:
				
				System.out.println("Introduce el nombre de una mision principal para saber si esta en la hermandad");
				programacionLovers.listarMisionPrincipal();
				
				String nombreBuscado = input.nextLine();
				
				System.out.println(programacionLovers.buscarMisionPrincipal(nombreBuscado));
				
			case 3:
				
				System.out.println("Introduce el nombre la mision principal para eliminarla");
				
				String nombreEliminado = input.nextLine();
				
				programacionLovers.eliminarMisionPrincipal(nombreEliminado);
				
				break;
				
			case 4:
				
				programacionLovers.listarMisionPrincipal();
				
				break;
			
			case 5:
				
				System.out.println("Ingresa el nombre de la mision principal que desea cambiar");
				String nombre = input.nextLine();
				
				System.out.println("INgresa de nuevo los datos de la mision principal");
				
				System.out.println("Ingrese el nuevo nombre");
				String nombreCambiar = input.nextLine();
				
				System.out.println("Ingrese la dificultad");
				String dificultad = input.nextLine();
				
				System.out.println("Ingrese el tiempo");
				int tiempo = input.nextInt();
				
				System.out.println("Ingrese la recompensa");
				String recompensa = input.nextLine();
				
				System.out.println("Ingrese el tipo");
				String tipo = input.nextLine();
				
				System.out.println("Ingrese la descripcion");
				String descripcion = input.nextLine();
				
				input.nextLine();
				
				programacionLovers.cambiarMisionPrincipal(nombre, nombreCambiar, dificultad, tiempo, recompensa, tipo, descripcion);
				
				break;
				
			case 6:
				
				System.out.println("Ingresa el nombre la mision principal para saber su informacion");
				String nombreInfo = input.nextLine();
				
				System.out.println(programacionLovers.buscarMisionPrincipal(nombreInfo));
				
				break;
				
			case 7:
				
				System.out.println("�Cuantas misiones secundarias quieres poner?");
				int numMisionSecunAlta = input.nextInt();
				
				input.nextLine();
				
				int totalMisionSecun= 1;
				
				for (int i = 0; i < numMisionSecunAlta; i++){
					
					if(numMisionSecunAlta <= numMisionSec) {
						
						System.out.println("Rellene los campos de la mision secundaria " + totalMisionSecun);
						
						System.out.println("Ingrese el nombre");
						String nombreS = input.nextLine();
						
						System.out.println("Ingrese la dificultad");
						String dificultadS = input.nextLine();
						
						System.out.println("Ingrese el tiempo");
						int tiempoS = input.nextInt();
						
						System.out.println("Ingrese la recompensa");
						String recompensaS = input.nextLine();
						
						System.out.println("Ingrese la experiencia obtenida");
						int experienciaObtenida = input.nextInt();
						
						System.out.println("Ingrese el oro obtenido");
						int oroObtenido = input.nextInt();
						
						input.nextLine();
						
						programacionLovers.altaMisionSecundaria(nombreS, dificultadS, tiempoS, recompensaS, experienciaObtenida, oroObtenido);
						totalMisionSecun++;
						
					}else {
						System.out.println("No se pueden a�adir mas misiones secundarias");
					}
				}
				
				break;
				
			case 8:
				
				programacionLovers.listarMisionSecundaria();
				
				break;
			
			case 9:
				
				System.out.println("Introduce el nombre la mision secundaria para eliminarla");
				
				String nombreSEliminado = input.nextLine();
				
				programacionLovers.eliminarMisionSecundaria(nombreSEliminado);
				
				break;
				
			case 10:
				
				System.out.println("�Cuantos personajes tiene va a tener la hermandad");
				int numPersonajesAlta = input.nextInt();
				input.nextLine();
				int totalPersonajes = 1;
				
				for (int i = 0; i < numPersonajesAlta; i++) {
					if(numPersonajesAlta <= numPers) {
						
						System.out.println("Ingresa los datos del personaje " + totalPersonajes);
						
						System.out.println("Introduce el nombre");
						String nombreP = input.nextLine();
						
						System.out.println("Introduce el nivel");
						int nivel = input.nextInt();
						
						input.nextLine();
						
						System.out.println("Introduce la clase");
						String clase = input.nextLine();
						
						programacionLovers.altaPersonaje(nombreP, nivel, clase);
						totalPersonajes++;
						
					}else {
						System.out.println("No se pueden ingresar mas personajes");
					}
				}
				
				break;
				
			case 11:
				
				System.out.println("Los personajes que tiene la hermandad son: ");
				programacionLovers.listarPersonaje();
				
				break;
				
			case 12:
				
				System.out.println("Cerrando el juego en 3, 2, 1....");
				
				break;
				
			default:
				
				System.out.println("Selecciona una opcion correcta por favor");
				
				break;
			}
			
		}while(condicion != 12);
		
		input.close();
		
	}
}
